class CreateUserVotes < ActiveRecord::Migration
  def change
    create_table :user_votes do |t|
      t.integer :voter_id, null: false
      t.integer :link_id, null: false
      t.integer :vote

      t.timestamps
    end
    add_index :user_votes, :voter_id
    add_index :user_votes, :link_id
  end
end
