class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :contributor_id
      t.string :title, null: false
      t.string :url
      t.text :body

      t.timestamps
    end

    add_index :links, :contributor_id
  end
end
