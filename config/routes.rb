RedditClone::Application.routes.draw do
  resources :users
  resource :session
  resources :subs
  resources :links do
    get "upvote", controller: :links
    get "downvote", controller: :links
  end
end
