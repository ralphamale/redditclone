module LinksHelper

  def link_contributor?
    @link = Link.find(params[:id])
    current_user.id == @link.contributor_id
  end

  def link_moderator?
    @link = Link.find(params[:id])
    @link.subs.any? {|sub| sub.moderator_id == current_user.id }
  end
end
