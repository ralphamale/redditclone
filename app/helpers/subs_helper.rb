module SubsHelper

  def subs_moderator?
    @sub = Sub.find(params[:id])
    current_user.id == @sub.moderator.id
  end
end
