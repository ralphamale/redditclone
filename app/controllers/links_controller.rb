class LinksController < ApplicationController
  include LinksHelper

  before_filter :ensure_link_contributor!, only: [:edit, :update]
  before_filter :ensure_deletion_capabilities, only: [:destroy]

  def new
    @link = Link.new
    @subs = Sub.includes(:links).all
  end

  def create
    @link = Link.new(params[:link])
    @link.contributor_id = current_user.id

    if @link.save
      redirect_to link_url(@link)
    else
      flash.now[:errors] = @link.errors.full_messages
      @subs = Sub.all
      render :new
    end
  end

  def show
    @link = Link.find(params[:id])
    render :show
  end

  def edit
    @subs = Sub.includes(:links).all
    @link = Link.find(params[:id])
  end

  def update
    @link = Link.find(params[:id])
    if @link.update_attributes(params[:link])
      redirect_to link_url(@link)
    else
      flash.now[:errors] = @link.errors.full_messages
      @subs = Sub.all
      render :edit
    end
  end

  def destroy
    @link = Link.find(params[:id])
    @link.destroy
    redirect_to subs_url
  end

  def upvote
    vote = UserVote.find_by_voter_id_and_link_id(current_user.id, params[:link_id])
    unless vote
      vote = UserVote.new(voter_id: current_user.id, link_id: params[:link_id], vote: 1)
      vote.save!
    else
      vote.update_attributes!(vote: 1)
    end
    redirect_to link_url(params[:link_id])
  end

  def downvote
    vote = UserVote.find_by_voter_id_and_link_id(current_user.id, params[:link_id])
    unless vote
      vote = UserVote.new(voter_id: current_user.id, link_id: params[:link_id], vote: -1)
      vote.save!
    else
      vote.update_attributes!(vote: -1)
    end
    redirect_to link_url(params[:link_id])
  end

  private

  def ensure_deletion_capabilities
    redirect_to link_url(@link) unless link_contributor? || moderator?
  end

  def ensure_link_contributor!
    @link = Link.find(params[:id])
    redirect_to link_url(@link) unless link_contributor?
  end

end
