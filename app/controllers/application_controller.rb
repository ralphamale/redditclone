class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user

  def login(user)
    user.reset_token
    session[:token] = user.token
  end

  def current_user
    @current_user ||= User.find_by_token(session[:token])
  end

  def ensure_logged_in!
    redirect_to new_session_url unless current_user
  end

end
