class SubsController < ApplicationController
  before_filter :ensure_logged_in!, only: [:new, :create]
  before_filter :ensure_moderator!, only: [:edit, :update, :destroy]

  def index
    @subs = Sub.all
  end

  def new
    @sub = Sub.new

    5.times do
      @sub.links.build
    end
  end

  def create
    @sub = Sub.new(params[:sub])
    @sub.moderator_id = current_user.id
    valid_link_params = params[:link].values.reject { |link| link["title"].empty? }
    @sub.links.new(valid_link_params)

    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def show
    @sub = Sub.includes(links: :contributor).find(params[:id])
    @links = @sub.links
  end

  def edit
    @sub = Sub.find(params[:id])
  end

  def update
    @sub = Sub.find(params[:id])

    if @sub.update_attributes(params[:sub])
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :edit
    end
  end

  def destroy
    @sub = Sub.find(params[:id])
    @sub.destroy
    redirect_to subs_url
  end

  private

  def ensure_moderator!
    @sub = Sub.find(params[:id])
    redirect_to sub_url(@sub) unless current_user.id == @sub.moderator_id
  end
end
