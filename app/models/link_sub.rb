# == Schema Information
#
# Table name: link_subs
#
#  id         :integer          not null, primary key
#  link_id    :integer          not null
#  sub_id     :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LinkSub < ActiveRecord::Base
  attr_accessible :link_id, :sub_id

  belongs_to :sub

  belongs_to :link

end
