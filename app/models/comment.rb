class Comment < ActiveRecord::Base
  attr_accessible :author_id, :body, :link_id, :parent_id

  validates :author_id, :body, :link_id, presence: true

  belongs_to :parent,
  class_name: "Comment"

  has_many :children,
  class_name: "Comment",
  foreign_key: :parent_id
end
