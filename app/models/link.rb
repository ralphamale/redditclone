# == Schema Information
#
# Table name: links
#
#  id             :integer          not null, primary key
#  contributor_id :integer
#  title          :string(255)      not null
#  url            :string(255)
#  body           :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Link < ActiveRecord::Base
  attr_accessible :body, :contributor_id, :title, :url, :sub_ids
  validates :title, presence: true

  belongs_to :contributor,
  class_name: "User"

  has_many :link_subs

  has_many :subs,
  through: :link_subs,
  source: :sub

  has_many :votes,
  class_name: "UserVote"

  def net_votes
    self.votes.sum(:vote)
  end


end
