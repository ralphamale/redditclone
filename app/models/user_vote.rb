# == Schema Information
#
# Table name: user_votes
#
#  id         :integer          not null, primary key
#  voter_id   :integer          not null
#  link_id    :integer          not null
#  vote       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserVote < ActiveRecord::Base
  attr_accessible :voter_id, :link_id, :vote

  validates :voter_id, :link_id, presence: true

  validates :vote, inclusion: { in: -1..1 }

  validates :voter_id, uniqueness: {scope: :link_id}

  belongs_to :link

  belongs_to :voter,
    class_name: "User"


end
