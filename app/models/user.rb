# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)      not null
#  password_digest :string(255)      not null
#  token           :string(255)      not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class User < ActiveRecord::Base
  attr_reader :password
  attr_accessible :username, :password
  before_validation :ensure_token

  has_many :links,
  foreign_key: :contributor_id

  has_many :link_subs

  has_many :subs,
  foreign_key: :moderator_id

  has_many :votes,
  foreign_key: :voter_id,
  class_name: "UserVote"


  def self.find_by_credentials(username, password)
    user = User.find_by_username(username)

    user && user.is_password?(password) ? user : nil

  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def ensure_token
    self.token ||= generate_token
  end

  def generate_token
    SecureRandom.urlsafe_base64(16)
  end

  def reset_token
    self.token = generate_token
    self.save!
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end
end
