# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    author_id 1
    link_id 1
    body Faker::Lorem.sentences
    parent_id nil


    trait :with_children do
      ignore do
        number_of_children 3
      end

      after :create do |parent_comment, evaluator|
        FactoryGirl.create_list :comment, evaluator.number_of_children, :parent_id => parent_comment.id
      end
    end
  end
end

