# == Schema Information
#
# Table name: user_votes
#
#  id         :integer          not null, primary key
#  voter_id   :integer          not null
#  link_id    :integer          not null
#  vote       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_vote do
    association :voter
    association :link
    vote 1
  end
end


