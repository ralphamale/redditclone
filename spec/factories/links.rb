# == Schema Information
#
# Table name: links
#
#  id             :integer          not null, primary key
#  contributor_id :integer
#  title          :string(255)      not null
#  url            :string(255)
#  body           :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :link do
    contributor

    title "MyString"
    url "MyString"
    body "MyText"

    factory :link_with_upvotes do
      after(:create) do |link|
        3.times do |n|
          link.votes << FactoryGirl.create(:user_vote, voter_id: (n+1), vote:
          1)
        end
      end

      factory :link_with_subs do
        after(:create) do |link|
          3.times do
            link.subs << FactoryGirl.create(:sub)
          end
        end
      end

    end
  end
end