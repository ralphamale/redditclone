require 'spec_helper'

describe Comment do
  subject(:comment) { FactoryGirl.create :comment, :with_children, :number_of_children => 4 }

  it "should be on a link"

  describe "child comment" do
    it "should have a parent comment" do
      expect(comment.children.first.parent).to eq(comment)
    end
  end

  describe "top level comment" do
    it "should not have a parent comment" do
      expect(comment.parent).to be_nil
    end

    it "may have children" do
      expect(comment.children.first).to be_a(Comment)
    end
  end
end
