# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)      not null
#  password_digest :string(255)      not null
#  token           :string(255)      not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'spec_helper'

describe User do

  subject(:user) { User.new(username: "Ralph", password: "password")}
  before { user.save! }

  it "should not have a password stored" do
    User.find_by_username("Ralph").password.should be_nil
  end

  it "should be able to be found by credentials" do
    expect(User.find_by_credentials("Ralph", "password")).to eq(user)
  end

  describe "#reset_session_token" do
    it "should change user's session token" do
      token = user.token
      user.reset_token
      expect(user.token).to_not eq(token)
    end
  end
end

