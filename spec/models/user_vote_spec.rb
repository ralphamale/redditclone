# == Schema Information
#
# Table name: user_votes
#
#  id         :integer          not null, primary key
#  voter_id   :integer          not null
#  link_id    :integer          not null
#  vote       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe UserVote do
    let(:link) { FactoryGirl.create(:link_with_upvotes) }
    let(:user) { FactoryGirl.create(:voter) }

    let(:vote) { vote = FactoryGirl.build(:user_vote, voter_id: user.id, link_id: link.id) }

    it "should change the vote count for the link" do
      net_votes_before = link.net_votes

      vote.save!

      expect(link.net_votes).to eq(net_votes_before + 1)
    end


    it "can be an upvote" do
      vote.vote = 1
      expect(vote).to be_valid
    end
    it "can be a downvote" do
      vote.vote = -1
      expect(vote).to be_valid
    end

    it "cannot be a random integer" do
      vote.vote = 5
      expect(vote).to_not be_valid
    end

    it "will not accept double votes" do
      vote.vote = -1
      expect(UserVote.new(voter_id: 1, link_id: link.id, vote: -1)).to_not be_valid
    end

end
