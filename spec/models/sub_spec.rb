# == Schema Information
#
# Table name: subs
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  moderator_id :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'spec_helper'

describe Sub do
  subject (:sub) { FactoryGirl.create(:sub_with_links) }

  it { should respond_to(:links) }
  it { should be_valid }

  it "should have many associated links" do
    expect(sub.links.first).to be_a(Link)
  end

end
