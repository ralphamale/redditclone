# == Schema Information
#
# Table name: links
#
#  id             :integer          not null, primary key
#  contributor_id :integer
#  title          :string(255)      not null
#  url            :string(255)
#  body           :text
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'spec_helper'

describe Link do

  subject(:link) { FactoryGirl.create(:link_with_subs) }

  it { should be_valid }

  it "should have a contributor" do
    expect(link.contributor).to be_a(User)
  end

  it "is associated with many subs" do
    expect(link.subs.first).to be_a(Sub)
  end

end
